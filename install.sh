#!/usr/bin/env bash

REPO=avskit
REPO_URL=https://ucic-1@bitbucket.org/ucic-1/$REPO.git

echo "Starting AVS Kit install"
curl -s 'http://avskit.ucic.io/installing' > /dev/null

cd ~
echo "Downloading..."
sudo apt-get -qq install git
git clone --depth 1 $REPO_URL
echo "Copying build to home directory"
mv $REPO/build/* ~
echo "Cleaning up..."
rm -rf $REPO
echo "Installing..."
bash setup-avs.sh

curl -s 'http://avskit.ucic.io/success-installing' > /dev/null
